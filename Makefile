# Executables
latexmk = lualatex -shell-escape

## Required for thumbpdf as latexmk does not support thumbpdf by itself
pdflatex = lualatex -shell-escape

## evince at linux
viewer = evince

## Editor
editor = kate

# Main file name
MASTER_TEX = base.tex
LITERATURE = refs.bib

# Derived file names
SRC = $(shell basename $(MASTER_TEX) .tex)
TEX_FILES = $(wildcard preambel/*.tex content/*.tex)
GFX_FILES = $(wildcard graphics/*)

PDF = $(SRC).pdf
AUX = $(SRC).aux

date=$(shell date +%Y%m%d%H%M)

all: $(PDF)
.PHONY: $(PDF)

$(PDF): $(MASTER_TEX) $(LITERATURE) $(TEX_FILES) $(GFX_FILES)
	$(latexmk) -pdf $(MASTER_TEX)

clean:
	$(latexmk) -C

final: $(PDF)
	thumbpdf $(PDF)
	$(pdflatex) $(MASTER_TEX)

mrproper: clean
	rm -f *~

pdf: $(PDF)

view: pdf
	$(viewer) $(PDF)&

edit:
	$(viewer) $(PDF)&
	$(editor) *.bib *.tex& 

.PHONY: exercises
exercises:
	lualatex -shell-escape ./exercises/0_solid/ex_solid
	lualatex -shell-escape ./exercises/1_review/ex_review
	lualatex -shell-escape ./exercises/2_examples/ex_examples
	lualatex -shell-escape ./exercises/3_error/ex_error
	lualatex -shell-escape ./exercises/4_embedded/ex_embedded
	lualatex -shell-escape ./exercises/5/ex_embedded_2
	lualatex -shell-escape ./exercises/6/ex_embedded_3
	lualatex -shell-escape ./exercises/7/ex_testing
	lualatex -shell-escape ./exercises/8/ex_system

